<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/datakaryawan','karyawancontroller@getdatakaryawan');

Route::get('/datakaryawan/{id}','karyawancontroller@getdatakaryawanbyperusahaanid');

Route::get('/dataperusahaan','perusahaancontroller@getdataperusahaan');

Route::get('/dataevent/{id}','eventcontroller@getdataeventbyid');

Route::post('/storeevent/{id}','eventcontroller@storeevent');

Route::get('dataevent','eventcontroller@getdataevent');

Route::post('/login','authcontroller@login');
Route::post('/register','authcontroller@register');
