<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\karyawan;
use Faker\Generator as Faker;

$factory->define(karyawan::class, function (Faker $faker) {
    return [
        'perusahaan_id' =>$faker ->numberBetween($min=1,$max=50),
        'name_karyawan' =>$faker ->name,
        'age' =>$faker ->numberBetween($min=18 , $max=65),
        'email' =>$faker ->email
    ];
});
