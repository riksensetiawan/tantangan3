<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\perusahaan;
use Faker\Generator as Faker;

$factory->define(perusahaan::class, function (Faker $faker) {
    return [
        'name' =>$faker->company
    ];
});
