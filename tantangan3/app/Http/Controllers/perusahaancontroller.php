<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class perusahaancontroller extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function getdataperusahaan()
    {
        $dataperusahaan = DB::table('perusahaans')->get();

        return response()->json([
            'status'=>200,
            'data' =>$dataperusahaan
        ]);
    }



    
}
