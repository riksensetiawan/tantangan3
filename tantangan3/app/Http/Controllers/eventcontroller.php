<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use\App\event;

class eventcontroller extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function getdataeventbyid($id)
    {
        $dataevent = DB::table('events')
        ->join('perusahaans','perusahaans.id','=','perusahaan_id')
        ->join('karyawans','karyawans.perusahaan_id','=','perusahaans.id')
        ->where('perusahaans.id','=',$id)
        ->select('perusahaans.name','name_event','name_karyawan')
        ->get();

        return response()->json([
            'data' => $dataevent
        ]);
    }

    public function getdataevent()
    {
        $dataevent = DB::table('events')
        ->get();
        return response()->json([
            'data'=>$dataevent
        ]);
    }

    public function storeevent(Request $request,$id)
    {
        event::create([
            'perusahaan_id' =>$request-> id,
            'name_event' =>$request -> name_event

        ]);

        return response()->json([
            'message' =>'sukses'
        ]);
 
    }
}
