<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use\App\User;
use Hash;
class authcontroller extends Controller
{
    public function login(Request $request)
    {
        $user =User::where('email',$request-> email)-> first();

        if($user)
        {
            if(Hash::check($request->password, $user->password))
            {
                $token =$user->CreateToken('Laravel Token');
                return response()->json([
                    'message'=>'success',
                    'token'=>$token->accessToken
                ]);
            }
            else
            {
                return response()->json([
                    'message' => 'password didnt match'
                ]);
            }
        }
        else
        {
            return response()->json([
                'message' => 'email not found'
            ]);
        }
    }

    public function register(Request $request)
    {
        $user=User::create([
            'name' => $request -> name,
            'password' => Hash::make($request->password),
            'email' => $request-> email
        ]);

        $token = $user->CreateToken('Laravel Token');
        return response()->json([
            'message' =>'success',
            'token' =>$token
        ]);
    }
}
