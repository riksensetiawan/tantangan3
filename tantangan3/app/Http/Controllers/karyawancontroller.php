<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class karyawancontroller extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function getdatakaryawanbyperusahaanid($id)
    {
        $datakaryawan = DB::table('karyawans')
        ->join('perusahaans','perusahaans.id','=','perusahaan_id')
        ->where('perusahaan_id','=',$id)
        ->select('name_karyawan','perusahaans.name','age','email')
        ->get();

        return response()->json([
            'status'=>200,
            'data' =>$datakaryawan
        ]);
    }

    public function getdatakaryawan()
    {
        $datakaryawan = DB::table('karyawans')->get();

        return response()->json([
            'status'=>200,
            'data' =>$datakaryawan

        ]);
    }
}
