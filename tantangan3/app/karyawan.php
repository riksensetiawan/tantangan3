<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class karyawan extends Model
{
    protected $fillable = ['perusahaan_id','name_karyawan','age','email'];
}
