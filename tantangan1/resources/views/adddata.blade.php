<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <title>Add</title>
</head>
<body>
<div class="navcontainer">
    <a href="{{route('home')}}">Home</a>
    <a href="\">Add New</a>
    <a href="{{route('edit_view')}}">Edit Existing</a>
    <a href="{{route('delete_view')}}">Delete</a>
</div>

<div>
    <form action="{{route('storedata')}}" method="POST">
        @csrf
        <p>insert name</p>
        <input type="text" name="insert_name">
        <br>
        <p>insert Email</p>
        <input type="text" name="insert_email">
        <input type="submit" value="submit">
    </form>
</div>
    
</body>
</html>